## AWS Lambda function that processes data
This is a simple lambda function that will process an HTTP request and return the square of a number. This function takes a JSON request containing an integer, calculates the square of that integer, and returns the result in JSON format.



## Deploy & Test
#### Deploy:
```
cargo lambda build
```
```
cargo lambda deploy --region us-east-1 --iam-role arn:aws:iam::339712859714:role/MiniProject2
```

#### Test API Gateway Integration:
After deploying this project to AWS, you can add a API Getway Trigger(HTTP API) and do the integration. Then you can test it with `curl` command.
```
curl -X POST https://utwfvswy1d.execute-api.us-east-1.amazonaws.com/test/square -H "Content-Type: application/json" -d '{"number":10}'
```

## API Gateway Integration:
![API](./API.jpg)

## AWS Console Test Result:
![Test](./Test.jpg)

## API Gateway Integration Test Result:
![Curl](./Curl.jpg)